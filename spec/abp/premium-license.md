# Premium license

Extension users who sign up for Premium are being given a license, which
identifies them as Premium users and unlocks Premium features.

## License activation

The license is activated after a successful payment occurred, which is signaled
on `https://accounts.adblockplus.org/%LANG%/premium` via a
["message" event][mdn-window-message], that contains the following data:

|Key|Value|Description|
|-|-|-|
|command|`"payment_success"`|API command|
|userId|string|Premium user ID string|
|version|`1`|API version|

In response, the extension should store the given Premium user ID,
[check the license](#license-check) and set up subsequent, daily
[license checks](#license-check). It should then emit its own "message" event to
notify the page, if there were no errors:

|Key|Value|Description|
|-|-|-|
|ack|`true`|API command to acknowledge|

While users have an active license, the extension should treat them as Premium
users and unlock Premium features for them.

## License check

The extension retrieves the license from
`https://myadblock.licensing.adblockplus.dev/license` by sending an HTTP POST
request with the following JSON data in the request body:

|Key|Value|Description|
|-|-|-|
|cmd|`"license_check"`|API command|
|u|string|Premium user ID string|
|v|`"1"`|API version|

If a license exists, the server responds with the following JSON data:

|Key|Value|Description|
|-|-|-|
|lv|`1`|License version|
|status|`"active"` or `"expired"`|License status|

The extension should then store the license and remove any existing ones, if
applicable.

If `status` is `"expired"`, the license should be
[deactivated](#license-deactivation).

If the server responds with an error, the extension should behave as follows:

|HTTP status code|Behavior|
|-|-|
|4XX|[Deactivate existing license](#license-deactivation)|
|5XX|Retry automatically up to three times in at least one minute gaps, and otherwise keep existing license active|

## License deactivation

If the license is no longer valid, the extension should remove the existing
license and Premium user ID and stop the daily license checks. It should also
stop treating them as a Premium user and lock Premium features for them.


[mdn-window-message]: https://developer.mozilla.org/en-US/docs/Web/API/Window/message_event
